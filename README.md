# agls4 - unofficial #

This repository is a fork of the Github repository [algs4](https://github.com/kevin-wayne/algs4). It is modified for [ALGator](https://github.com/ALGatorDevel/Algator) project to provide serializable support. Not all of the classes are edited. For the development which is not related to ALGator, please use source code of the official repository.

Use pull requests for any changes.

### License ###

This code is released under GPLv3.
